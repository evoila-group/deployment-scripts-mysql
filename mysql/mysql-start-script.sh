#!/bin/bash

export REPOSITORY_MYSQL="https://bitbucket.org/meshstack/deployment-scripts-mysql/raw/HEAD/mysql"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_MYSQL/mysql-template.sh --no-cache
chmod +x mysql-template.sh
./mysql-template.sh -d evoila -p evoila -e openstack
