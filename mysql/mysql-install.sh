#!/bin/bash

#starts with parameters for database name and root password set in mysql-template.sh
echo "mysql_database = ${MYSQL_DATABASE}"
echo "datadir = ${DATADIR}"
echo "environment = ${ENVIRONMENT}"
echo "repository_mysql = ${REPOSITORY_MYSQL}"
echo "check_path = ${CHECK_PATH}"


# adds custom apparmor-profile for mysql
echo "# vim:syntax=apparmor
# Last Modified: Thu Nov 05 12:00:00 2015
#include <tunables/global>

/usr/sbin/mysqld {
  #include <abstractions/base>
  #include <abstractions/nameservice>
  #include <abstractions/user-tmp>
  #include <abstractions/mysql>
  #include <abstractions/winbind>

  # Allow system resource access
  /sys/devices/system/cpu/ r,
  capability sys_resource,
  capability dac_override,
  capability setuid,
  capability setgid,
  capability chown,

  network tcp,

  # Allow config access
  /etc/mysql/conf.d/ r,
  /etc/mysql/conf.d/*.cnf r,
  /etc/mysql/*.cnf r,

  /etc/mysql/*.pem r,
  /etc/mysql/conf.d/* r,
  /etc/mysql/mysql.conf.d/* r,
  /etc/mysql/mysql.conf.d/ r,


  /etc/hosts.allow r,
  /etc/hosts.deny r,

  # Allow pid, socket, socket lock file access
  /var/run/mysqld/mysqld.pid rw,
  /var/run/mysqld/mysqld.sock rw,
  /var/run/mysqld/mysqld.sock.lock rw,
  /run/mysqld/mysqld.pid rw,
  /run/mysqld/mysqld.sock rw,
  /run/mysqld/mysqld.sock.lock rw,

  # Allow read/ write to /tmp
  /tmp/ r,
  /tmp/* rw,

  # Allow execution of server binary
  /usr/sbin/mysqld mr,
  /usr/sbin/mysqld-debug mr,

  # Allow plugin access
  /usr/lib/mysql/plugin/ r,
  /usr/lib/mysql/plugin/*.so* mr,

  # Allow error msg and charset access
  /usr/share/mysql/ r,
  /usr/share/mysql/** r,

  # Allow data dir access
  ${DATADIR}/ rw,
  ${DATADIR}/** rwk,

  /var/lib/mysql/ rw,
  /var/lib/mysql/** rwk,

  # Allow data files dir access
  /var/lib/mysql-files/ r,
  /var/lib/mysql-files/** rwk,
  ${DATADIR}/ r,

  # Allow log file access
  /var/log/mysql/ r,
  /var/log/mysql/** rw,
  /var/log/mysql.err rw,
  /var/log/mysql.log rw,
  /var/log/mysql/* rw,

  /sys/devices/system/node/ r,
  /sys/devices/system/node/** rwk,
  /sys/devices/system/node/node0/meminfo r,

  /proc/ r,
  /proc/** rwk,
  /proc/*/status r,

  /usr/sbin/mysqld r,
  /usr/sbin/mysqld** rwk,
}
" > /etc/apparmor.d/usr.sbin.mysqld

# reload apparmor with new custom profile for mysql
apparmor_parser -r /etc/apparmor.d/usr.sbin.mysqld
/etc/init.d/apparmor restart

# adds userdirectory for user mysql
mkdir -p /home/mysql
# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
groupadd -r mysql && useradd -r -g mysql mysql
chown -R mysql:mysql /home/mysql
chmod 760 -R /home/mysql


mkdir /docker-entrypoint-initdb.d

# FATAL ERROR: please install the following Perl modules before executing /usr/local/mysql/scripts/mysql_install_db:
# File::Basename
# File::Copy
# Sys::Hostname
# Data::Dumper
apt-get update && apt-get install -y perl --no-install-recommends && rm -rf /var/lib/apt/lists/*

# gpg: key 5072E1F5: public key "MySQL Release Engineering <mysql-build@oss.oracle.com>" imported
apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys A4A9406876FCBD3C456770C88C718D3B5072E1F5


echo "deb http://repo.mysql.com/apt/ubuntu/ trusty mysql-${MYSQL_MAJOR}" > /etc/apt/sources.list.d/mysql.list

# the "/var/lib/mysql" stuff here is because the mysql-server postinst doesn't have an explicit way to disable the mysql_install_db codepath besides having a database already "configured" (ie, stuff in /var/lib/mysql/mysql)
# also, we set debconf keys to make APT a little quieter
debconf-set-selections <<< "mysql-community-server mysql-community-server/data-dir select ''"
debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password ops90r!st8"
debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password ops90r!st8"
debconf-set-selections <<< "mysql-community-server mysql-community-server/remove-test-db select false"
apt-get update

apt-get install -y -o Dpkg::Options::="--force-confold" mysql-server="${MYSQL_VERSION}"

rm -rf /var/lib/apt/lists/* \
rm -rf ${DATADIR} && mkdir -p ${DATADIR}

chown -R mysql:mysql ${DATADIR}
chmod 760 -R ${DATADIR}

# comment out a few problematic configuration values
# don't reverse lookup hostnames, they are usually another container
sed -Ei 's/^(bind-address|log)/#&/' /etc/mysql/my.cnf \
&& echo -e 'skip-host-cache\nlog-error      = /var/log/mysql/error.log' | awk '{ print } $1 == "[mysqld]" && c == 0 { c = 1; system("cat") }' /etc/mysql/my.cnf > /tmp/my.cnf \
&& mv /tmp/my.cnf /etc/mysql/my.cnf

#&& echo 'skip-name-resolve' | awk '{ print } $1 == "[mysqld]" && c == 0 { c = 1; system("cat") }' /etc/mysql/my.cnf > /tmp/my.cnf \
#&& mv /tmp/my.cnf /etc/mysql/my.cnf

# stopps mysql
# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
 $OPENSTACK_STOP
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_STOP
fi
